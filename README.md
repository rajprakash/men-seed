# MEN Seed

.env file sample

# Set to production when deploying to production
NODE_ENV=development

# Node.js server configuration
SERVER_PORT=3000

# mongodb configuration
DB_END_POINT=your db url 