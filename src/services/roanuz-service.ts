
import * as request from 'request';
import Config from '../config';
import Cacher from '../cacher';
import * as querystring from 'querystring';
import { constants } from 'os';

class RoanuzService {

    token = '2s14381397325301s842646890607683524';
    host = Config.backend.host;
    lastQuery = {};
    formateUrl = '';
    headers: any;
    memCacheEnable = Config.enable_memcache;
    
    constructor() {
        this.formateUrl = '',
        this.headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }

    getAccessToken() {
        return new Promise(resolve => {
            var options = {
                url: this.host+'/rest/v2/auth/',
                method: 'POST',
                json:true,
                gzip: true,
                form: Config.auth
            }

            request(options, (error, response, body) => {
                if(error) console.log(error);
                else {
                    if(body.status_code == 200){
                        resolve({
                            token :  body.auth.access_token
                        })
                        console.log('body.auth.access_token',body.auth.access_token);
                    }
                    else{
                        resolve(false);
                    }
                };
            });
        });
    }

    setAccessToken(){
        return new Promise(resolve => {
            this.getAccessToken().then((data) => {
                if(data){
                    const gotToken: any = data;
                    this.token = gotToken.token;
                    resolve(true);
                }
            })
        });
    }

    recentMatchesResponse(){
        const apiPath = '/rest/v2/recent_matches/';
        const params = {
            card_type: 'summary_card'
        };
        return new Promise((resolve, reject) => {
            resolve(this.getData(apiPath, params));
        }).catch((err) => {
            console.error('Oops we have an error', err);
            // reject(err);
        });
    }

    scheduleResponse(month){
        const apiPath = '/rest/v2/schedule/';
        const params = {};
        if (month) {
            params['date'] = month
        }
        return new Promise((resolve, reject) => {
            resolve(this.getData(apiPath, params));
        }).catch(function(err) {
            console.error('Oops we have an error', err);
            // reject(err);
        });
    }

    getMatchResponse(matchId){
        const apiPath = '/rest/v2/match/'+matchId+'/';
        const params = {}
        const cacheKey = 'match|'+matchId+'|full_card';
        return new Promise((resolve, reject) => {
            if (this.memCacheEnable) {
                Cacher.getCachedData(cacheKey).then((cachedResponse)=>{
                    if(cachedResponse&& cachedResponse != null) {
                        resolve(cachedResponse)
                    } else {
                        resolve(this.getData(apiPath, params));
                    }
                });
            } else {
                resolve(this.getData(apiPath, params));
            }
        }).catch(function(err) {
            console.error('Oops we have an error', err);
            // reject(err);
        });
    }

    getData(source_path, queryParams: any ={}){
        console.log('getdata called')
        // let newHost = host
        // if (source_path.startsWith('/rest/add-on/spider-')) {
        //     newHost = Config.backend.spiderHost
        // }

        return new Promise((resolve, reject) => {
            if(queryParams.hasOwnProperty("access_token")) {
               queryParams.access_token = this.token;
            } else {
                queryParams['access_token'] = this.token;
            }
            
            this.formateUrl = this.host + source_path;
            
            var options = {
                url: this.formateUrl,
                qs:queryParams,
                method: 'GET',
                headers: this.headers,
                json:true,
                gzip: true
            }
            console.log(options)
            
            request(options, (error, response, body) => {
                if(error) {
                    console.log(error)
                    reject(error);
                } else {
                    if(body.status_code == 403 && (body.status_msg == 'Invalid Access Token' || body.status_msg == 'App Not Found')){   
                        this.lastQuery = queryParams                      
                        this.setAccessToken().then(() => {                            
                            resolve(this.getData(source_path, this.lastQuery));
                        }).catch((res) => {
                            console.log("catch",res);
                        })
                    } else if(body.status_code == 403 && body.status_msg == 'InvalidAccessToken'){       
                        this.lastQuery = queryParams        
                        this.setAccessToken().then(() => {
                            resolve(this.getData(source_path, this.lastQuery));
                        }).catch((res) => {
                            console.log("catch",res);
                        })
                    } else {
                        if(body.status_code == 200 && this.memCacheEnable){
                            if (body.data.card && body.data.card.cache_key){
                                Cacher.setCache(body.data.card.cache_key, body.data, body.expires);
                            }
                        }
                        resolve(body.data);
                    }                    
                }
            })
        })
    }

}

export default new RoanuzService;