import app from "./server";
import config  from "./config";

app.listen(config.port,'0.0.0.0', () => {
    console.log('server listening on port ' + config.port);
});