import { Request, Response } from 'express';
import RoanuzService from '../services/roanuz-service'

export class ScoreController {

    public schedule(req: Request, res: Response) {
        RoanuzService.scheduleResponse(null).then((response) => {
            res.json(response);
        }).catch((err) => {
            console.error('Oops we have an error', err);
        })
    }

    public monthWiseSchedule(req: Request, res: Response) {
        RoanuzService.scheduleResponse(req.params['month']).then((response) => {
            res.json(response);
        }).catch((err) => {
            console.error('Oops we have an error', err);
        })
    }

    public recentMaches(req: Request, res: Response) {
        RoanuzService.recentMatchesResponse().then((response) => {
            res.json(response);
        }).catch((err) => {
            console.error('Oops we have an error', err);
        })
    }

    public matchDetails(req: Request, res: Response) {
        RoanuzService.getMatchResponse(req.params['matchId']).then((response) => {
            res.json(response);
        }).catch((err) => {
            console.error('Oops we have an error', err);
        })
    }



}
