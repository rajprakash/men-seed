import * as socketIoClient from 'socket.io-client';
import * as request from 'request';
import * as https from 'https';
import * as  util from 'util';
import config from '../config';

export class RoanuzSocket {
    pushServers = [];
    authToken = null;
    io: any;

    constructor() { }

    public connectSocket(io) {
        this.io = io;
        this.auth((result) => {

            if (result.auth.push_servers) {
                this.pushServers = result.auth.push_servers;
                this.authToken = result.auth.access_token;
                this.connectSocketClient();
            } else if (!result.auth) {
                console.log('Auth failed.');
            } else {
                console.log('Push notification is not enabled for your app.');
            }

        });
    }

    private connectSocketClient(): void {

        const server = this.pushServers[0];
        const host = 'http://' + server.host + ':' + server.port;
        console.log('Connecting', host + '/stream');

        const socket = socketIoClient.connect(host + '/stream');

        socket.on('auth_failed', () => {
            console.log('Auth Failed, consider using new access token. And make sure you have access to the connecting match.');
        });

        socket.on('match_update', (card) => {
            console.log('Got a match update for', card);
            this.io.emit('card', card);
        });


        socket.on('event', () => {
            console.log('event');
        });

        socket.on('error', () => {
            console.log('error');
        });

        socket.on('connect', () => {
            console.log('Stream Connected');
            this.getData('recent_matches/').then((dataResponse: any) => {
                if (dataResponse && dataResponse != null) {
                    console.log('recentMatches', dataResponse.intelligent_order)
                    let recentMatches = [];
                    if (dataResponse && dataResponse.cards)
                        recentMatches = dataResponse.cards;
                    if (recentMatches.length > 0) {
                        for (let i = 0; i < recentMatches.length; i++) {
                            console.log(i, recentMatches[i].key)
                            if (recentMatches[i].status == 'started') {
                                // socket.emit('auth_match', { 'match': recentMatches[i].key, 'access_token': this.authToken });
                            }
                            socket.emit('auth_match', { 'match': recentMatches[i].key, 'access_token': this.authToken });
                        }
                    }
                }
            }).catch((err) => {
                // This will get hit!
                console.error('Oops we have an error', err);
            })
        });
        socket.on('disconnect', () => {
            console.log('Stream disconnected');
        });

        socket.on('connect_error', (err) => {
            console.log('conntection failed');
        });



    }

    private auth(onAuth: any) {
        const data = util.format(
            'access_key=%s&secret_key=%s&app_id=%s&device_id=%s',
            config.auth.access_key, config.auth.secret_key, config.auth.app_id, config.auth.device_id);
        const post = {
            host: config.scApi.host,
            port: config.scApi.port,
            path: config.scApi.prefix + 'auth/',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': data.length
            }
        }

        const req = https.request(post, (res) => {
            res.setEncoding('utf8');
            res.on('data', (result) => {
                onAuth.call(undefined, JSON.parse(result));
            });
        });

        req.write(data);
        req.end();
    }

    getData(source_path, queryParams: any = {}) {
        return new Promise((resolve, reject) => {
            if (queryParams.hasOwnProperty("access_token")) {
                queryParams.access_token = this.authToken;
            } else {
                queryParams['access_token'] = this.authToken;
            }

            const reqUrl = 'https://rest.cricketapi.com/rest/v2/' + source_path;

            const options = {
                url: reqUrl,
                qs: queryParams,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                json: true,
                gzip: true
            }
            request(options, (error, response, body) => {
                if (error) {
                    console.log(error)
                    reject(error);
                } else {
                    // console.log('resData', body.data)
                    resolve(body.data);
                }
            })
        })
    }



}