import * as dotenv from 'dotenv';

class AppConfig {
    
    backend: any;
    auth: any;
    port: number;
    mongo: any;
    scApi: any;
    enable_memcache = false;

    constructor() {
        dotenv.config();

        this.port = this.envFor('PORT', 3000);

        this.backend = {
            host: 'https://rest.cricketapi.com',
            yardsHost: 'http://localhost:3000',
            path: '/',
        };
        
        this.auth = {
            app_id: this.envFor('APP_ID'), // get it from .env
            access_key: this.envFor('ACCESS_KEY'),
            secret_key: this.envFor('SECRET_KEY'),
            device_id: this.envFor('DEVICE_ID')
        };

        this.scApi = {
            host: this.envFor('API_HOST'),
            port: this.envFor('API_PORT'),
            prefix: this.envFor('API_PREFIX')
        };

        this.mongo = {
            endPoint: this.envFor('DB_END_POINT')
        };

        if (this.envFor("ENABLE_MEMCACHE", false) == 'true'){
            this.enable_memcache = true;
        } else {
            this.enable_memcache = false;
        }
    }

    envFor(name, defaultValue = null){
        if (process.env[`${name}`] != undefined) {
            return process.env[`${name}`]
        }
        return defaultValue
    }

    
    
}

export default new AppConfig();