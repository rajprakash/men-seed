import {Request, Response} from "express";
import { ScoreController } from '../controllers/scoreController';
import * as path from "path";

export class Routes { 
    public scoreCtrl: ScoreController = new ScoreController();      
    public routes(app): void {
        
        app.route('/').get((req: Request, res: Response) => {
            res.sendFile(path.resolve("./client/index.html"));
        });
          
        app.route('/schedule')
        .get(this.scoreCtrl.schedule);  
        
        app.route('/schedule/:month')
        .get(this.scoreCtrl.monthWiseSchedule); 

        app.route('/recent-matches')
        .get(this.scoreCtrl.recentMaches);

        app.route('/match/:matchId')
        .get(this.scoreCtrl.matchDetails);
    }
}