import * as MemcachePlus  from 'memcache-plus';

class Cacher {
    client = new MemcachePlus();

    constructor() {}

    setCache(key, value, ttl) {
        const toDate = new Date();
        const timestamp = toDate.getTime(); // in timestamp
        let cache_expires = 120;
        if(ttl>timestamp){
            cache_expires = ttl - timestamp / 1000;
        }
        this.client
            .set(key, value, cache_expires)
            .then(() => {
                // This will never happen because an error will be thrown
                console.log('Succeffuly cached data for', key);
            })
            .catch((err) => {
                // This will get hit!
                console.error('Oops we have an error', err);
            }
        );
    }

    getCachedData(key) {
        return new Promise((resolve,reject)=>{
            this.client
                .get(key)
                .then((data) => {
                    // The user is a JS object:
                    if(data && data.card){
                        console.log('Successfully got the object', data.card.title);
                        resolve(data);
                    }
                    else{
                        resolve(false)
                    }
                    
                })
                .catch((err) => {
                    // This will get hit!
                    console.error('Oops we have an error', err);
                    reject(err);
                });
        });
    }

}

export default new Cacher();