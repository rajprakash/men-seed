import * as express from 'express';
import { createServer, Server } from 'http';
import * as socketIo from 'socket.io';
import * as bodyParser from 'body-parser';
import * as mongoose from 'mongoose';
import { Routes } from './routes/scoreRoutes';
import config from './config';
import {RoanuzSocket} from './roanuz-socket/socket-client';

class App {
  public app: express.Application;
  public server: Server;
  public io: SocketIO.Server;
  public routePrv: Routes = new Routes();
  public roanuzSocket: RoanuzSocket = new RoanuzSocket();

  constructor() {
    this.app = express();
    this.config();
    this.sockets();
    this.mongoConfig();
    this.routePrv.routes(this.app);
  }

  private config(): void {
    // create server
    this.server = createServer(this.app);
    // support application/json type post data
    this.app.use(bodyParser.json());
    // support application/x-www-form-urlencoded post data
    this.app.use(bodyParser.urlencoded({ extended: false }));
    // cross orgin support
    this.app.use((req, res, next) => {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
      next();
    });
  }

  private mongoConfig(): void {
    mongoose.Promise = global.Promise;
    mongoose.connect(config.mongo.endPoint, { useNewUrlParser: true });
  }

  private sockets(): void {
    this.io = socketIo(this.server);
    // this.roanuzSocket.connectSocket(this.io);
    this.io.on('connection', (socket: any) => {
      console.log('a user is connected');
      socket.on('message', (message: any) => {
        console.log(message);
      });
      socket.on('disconnect', () => {
        console.log('Client disconnected');
      });
    });
  }

  }

export default new App().server;
